require('dotenv').config()

const express = require('express')
const cors = require('cors')
const app = express()
const morganBody = require('morgan-body')
const loggerStream = require('./utils/handleLogger')
const bookingStream = require('./utils/handleBooking')
const dbConnect = require('./config/mongo')

app.use(cors())
app.use(express.json())

morganBody(app, {
  noColors: true,
  stream: loggerStream,
  skip: function (req, res) {

    return res.statusCode < 400
  }

})

morganBody(app, {
  noColors: true,
  stream: bookingStream,
  skip: function (req, res) {
    console.log(res.statusCode)
    return ( res.baseUrl == '/api/booking' && res.statusCode >= 400)
  }
})

const port = process.env.PORT || 3000



// Call routes

app.use('/api', require('./routes'))

app.listen(port, () => {
  console.log(`Tu app esta lista por http://localhost:${port}`)
})

dbConnect()