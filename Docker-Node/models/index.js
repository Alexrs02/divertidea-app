const models = {
    usersModel: require('./nosql/users'),
    bookingsModel: require('./nosql/bookings')
}

module.exports = models