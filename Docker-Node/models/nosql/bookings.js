const mongoose = require('mongoose')

const BookingScheme = new mongoose.Schema(
    {
        name: {
            type: String
        },
        kidname: {
            type: String
        },
        kidage: {
            type: Number
        },
        accepted: {
            type: ["yes", "no"],
            default: "no"
        },
        date: {
            type: String
        },
        quantity: {
            type: Number
        },
        userid: {
            type: String
        }
    },
    {
        timestamps: true, // createdAt, updatedAt
        versionKey: false
    }
)

module.exports = mongoose.model('bookings', BookingScheme)