const express = require("express")
const { getItems, getItem, createItem, updateItem, deleteItem } = require("../controllers/users")
const authMiddleWare = require("../middleware/session")
const { validatorRegister } = require("../validators/auth")
const router = express.Router()

router.put('/:id', authMiddleWare, updateItem)
router.delete('/:id', authMiddleWare, deleteItem)


module.exports = router