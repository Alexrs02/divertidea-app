const express = require("express")
const { getItems, getItem, createItem, updateItem, deleteItem } = require("../controllers/bookings")
const { validatorCreateItem, validatorGetItem } = require('../validators/bookings')
const authMiddleWare = require('../middleware/session')
const customHeader = require('../middleware/customHeader')
const checkRol = require("../middleware/role")
const router = express.Router()

// http://localhost/bookings GET, POST, DELETE, PUT

router.get('/', authMiddleWare, getItems)
router.put('/:id', authMiddleWare, updateItem)
router.get('/:userid', authMiddleWare, validatorGetItem, getItem)
router.post('/',
    authMiddleWare,
    checkRol(["user"]),
    validatorCreateItem,
    createItem)
router.delete('/:id', authMiddleWare, deleteItem)

module.exports = router