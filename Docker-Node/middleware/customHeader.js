const customHeader = (req, res, next) => {
    try {
        const apiKey = req.headers.api_key
        if(apiKey === "leifer-01"){
            next()
        }else{
            res.status(403)
            res.send({error: "API_KEY_WRONG"})
        }
    } catch (e) {
        res.status(403)
        res.send({error: "SOMETHING_GO_WRONG_ON_CUSTOM_HEADER"})
    }
}

module.exports = customHeader