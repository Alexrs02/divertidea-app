const { matchedData } = require('express-validator')
const { usersModel } = require('../models')
const { encrypt } = require('../utils/handlePassword')
const { tokenSign } = require('../utils/handleJwt')
const { handleHttpError } = require('../utils/handleError')
const { compare } = require('bcryptjs')
const moment = require('moment')



const registerCtrl = async (req, res) => {
    try {
        req = matchedData(req)

        const phone = await usersModel.findOne({ phone: req.phone }).select('phone')
        if (phone) {
            handleHttpError(res, "PHONE_IN_USE", 401)
            return
        }

        const email = await usersModel.findOne({ email: req.email }).select('email')
        if(email){
            handleHttpError(res, "EMAIL_IN_USE", 401)
            return
        }

        const password = await encrypt(req.password)
        const body = { ...req, password }
        const dataUser = await usersModel.create(body)
        dataUser.set('password', undefined, { strict: false })

        const data = {
            token: await tokenSign(dataUser),
            user: dataUser
        }
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}

const loginCtrl = async (req, res) => {
    try {
        req = matchedData(req)
        const user = await usersModel.findOne({ email: req.email }).select('password name role email phone date')
        if (!user) {
            handleHttpError(res, "USER_NOT_EXIST", 404)
            return
        }

        const hashPasswd = user.get('password')
        const check = await compare(req.password, hashPasswd)
        if (!check) {
            handleHttpError(res, "PASSWORD_WRONG", 401)
            return
        }

        user.set('password', undefined, { strict: false })
        const data = {
            token: await tokenSign(user),
            user
        }
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}

/**
 * Obtain list 
 * @param {*} req 
 * @param {*} res 
 */
const getItems = async (req, res) => {
    const data = await usersModel.find({})
    res.send({ data })
}
/**
 * Obtain detail
 * @param {*} req 
 * @param {*} res 
 */
const getItem = (req, res) => {

}


/**
 * Update
 * @param {*} req 
 * @param {*} res 
 */
const updateItem = async (req, res) => {
    try {
        const itemId = req.params.id; // Suponiendo que 'id' es el parámetro para obtener el ID del elemento a actualizar
        const { name, email,phone,date } = req.body;
        const data = await usersModel.findByIdAndUpdate(
            itemId,
            {
                name,
                email,
                phone,
                date
            },
            { new: true } // Para obtener el elemento actualizado en lugar del original
        );
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}
/**
 * Delete
 * @param {*} req 
 * @param {*} res 
 */
const deleteItem =  async (req, res) => {
    try {
        req = matchedData(req)
        const { id } = req
        const data = await usersModel.deleteOne({_id:id})
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}

module.exports = {
    getItems,
    getItem,
    updateItem,
    deleteItem,
    registerCtrl,
    loginCtrl
}