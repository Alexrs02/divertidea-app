const { matchedData } = require('express-validator')
const { bookingsModel } = require('../models')
const { handleHttpError } = require('../utils/handleError')

/**
 * Obtain list 
 * @param {*} req 
 * @param {*} res 
 */
const getItems = async (req, res) => {
    try {
        const user = req.user
        const data = await bookingsModel.find({})
        res.send({ data, user })
    } catch (e) {
        handleHttpError(res, "ERROR_GET_ITEMS")
    }
}
/**
 * Obtain detail
 * @param {*} req 
 * @param {*} res 
 */
const getItem = async (req, res) => {
    try {
        req = matchedData(req)
        const { userid } = req
        const data = await bookingsModel.find({ userid: userid })
        res.send({ data })
    } catch (e) {
        handleHttpError(res, "ERROR_GET_ITEM")
    }
}

/**
 * Insert
 * @param {*} req 
 * @param {*} res 
 */
const createItem = async (req, res) => {
    try {
        req = matchedData(req)
        const date = await bookingsModel.findOne({ date: req.date }).select('date')
        if (date) {
            handleHttpError(res, "DATE_IN_USE", 401)
            return
        }
        const data = await bookingsModel.create(req)
        res.send({ data })
    } catch (e) {
        handleHttpError(res, "ERROR_POST_ITEM")
    }
}

/**
 * Update
 * @param {*} req 
 * @param {*} res 
 */
const updateItem = async (req, res) => {
    try {
        const itemId = req.params.id; // Suponiendo que 'id' es el parámetro para obtener el ID del elemento a actualizar
        const { accepted } = req.body;
        const data = await bookingsModel.findByIdAndUpdate(
            itemId,
            {
                accepted,
            },
            { new: true } // Para obtener el elemento actualizado en lugar del original
        );
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}
/**
 * Delete
 * @param {*} req 
 * @param {*} res 
 */
const deleteItem = async (req, res) => {
    try {
        req = matchedData(req)
        const { id } = req
        const data = await bookingsModel.deleteOne({ _id: id })
        res.send({ data })
    } catch (e) {
        handleHttpError(res, e)
    }
}

module.exports = {
    getItems,
    getItem,
    createItem,
    updateItem,
    deleteItem
}