const {IncomingWebhook} = require('@slack/webhook')
const webhook = new IncomingWebhook(process.env.SLACK_BOOKING_WEBHOOK)


const bookingStream = {
    write: message => {
        // console.log(message)
        webhook.send({
            text: message
        })
    },
};

module.exports = bookingStream