const bcryptjs = require('bcryptjs')

/**
 * Convert password to a encrypt password
 * @param {*} passwd 
 * @returns 
 */
const encrypt = async (passwd) => {
    const hash = await bcryptjs.hash(passwd, 3)
    return hash
}


/**
 * Password without encrypt and password encrypt
 * @param {*} passwd 
 * @param {*} hash 
 */
const compare = async (passwd, hash) => {
    return await bcryptjs.compare(passwd, hash)
}

module.exports = {
    encrypt, compare
}