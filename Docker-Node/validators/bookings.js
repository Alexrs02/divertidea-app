const { check } = require('express-validator')
const validateResults = require('../utils/handleValidator')

const validatorCreateItem = [
    check('name').exists().notEmpty(),
    check('kidname').exists().notEmpty(),
    check('kidage').exists().notEmpty(),
    check('accepted').exists().notEmpty(),
    check('date').exists().notEmpty(),
    check('quantity').exists().notEmpty(),
    check('userid').exists().notEmpty(),

    (req, res, next) => {
        return validateResults(req, res, next)
    }

]

const validatorGetItem = [
    check('userid').exists().notEmpty(),
    (req, res, next) => {
        return validateResults(req, res, next)
    }

]

module.exports = { validatorCreateItem, validatorGetItem }