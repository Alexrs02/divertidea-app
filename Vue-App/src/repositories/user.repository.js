import axios from 'axios';

export class UserRepository{
    async putUser(id,name,email,phone,date,token){
        try {
            const resp = await axios({
                method: 'PUT',
                url: `http://localhost:3001/api/users/${id}`,
                data: {
                    name: name,
                    email: email,
                    phone: phone,
                    date: date
                },
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const data = resp.data.data
            return {state: resp.status, user: data }
        } catch (error) {
            const data = error.response.data.error
            return data
        }
    }
}