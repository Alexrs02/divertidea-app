import axios from 'axios';


export class BookingRepository {
    async getBookings(userid, token) {
        try {
            const resp = await axios({
                method: 'GET',
                url: `http://localhost:3001/api/bookings/${userid}`,
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const bookingsData = resp.data.data
            return { state: resp.status, bookings: bookingsData }
        } catch (error) {
            const data = error.response.data.error
            return { state: data, bookings: null }
        }
    }

    async getAllBookings(token) {
        try {
            const resp = await axios({
                method: 'GET',
                url: 'http://localhost:3001/api/bookings',
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            const bookingsData = resp.data.data
            return { state: resp.status, bookings: bookingsData }
        } catch (error) {
            const data = error.response.data.error
            return { state: data, bookings: null }
        }
    }

    async postBooking(booking, token) {
        try {
            const resp = await axios({
                method: 'POST',
                url: 'http://localhost:3001/api/bookings',
                data: {
                    name: booking.name,
                    kidname: booking.kidname,
                    accepted: booking.accepted,
                    kidage: booking.kidage,
                    date: booking.date,
                    quantity: booking.quantity,
                    userid: booking.userid
                },
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            return resp.status
        } catch (error) {
            const data = error.response.data.error
            return data
        }
    }

    async putManageBooking(accepted, id, token) {
        try {
            const resp = await axios({
                method: 'PUT',
                url: `http://localhost:3001/api/bookings/${id}`,
                data: {
                    accepted: accepted
                },
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            return resp.status
        } catch (error) {
            const data = error.response.data.error
            return data
        }
    }
}