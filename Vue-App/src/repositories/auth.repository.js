import axios from 'axios';

export class AuthRepository {
    async loginUser(email, passwd) {
        try {
            const resp = await axios({
                method: 'POST',
                url: 'http://localhost:3001/api/auth/login',
                data: {
                    email: email,
                    password: passwd
                }
            })
            const data = resp.data
            const token = data.data.token
            const dataUser = data.data.user
            localStorage.setItem('token', token)
            return {state: resp.status, user: dataUser }
        } catch (error) {
            const data = error.response.data.error
            return {state: data, user: null}
        }
    }

    async registerUser(name, email, passwd, phone, date) {
        try {
            const resp = await axios({
                method: 'POST',
                url: 'http://localhost:3001/api/auth/register',
                data: {
                    name: name,
                    email: email,
                    password: passwd,
                    phone: phone,
                    date: date
                }
            })
            return resp.status
        } catch (error) {
            const data = error.response.data.error
            return data
        }
    }
}