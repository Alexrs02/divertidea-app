import { AuthRepository } from "../repositories/auth.repository";

export class LogInUseCase {
    async execute(email, passwd) {
        const repository = new AuthRepository()
        const state = await repository.loginUser(email, passwd)
        return state
    }
}

export class SignInUseCase {
    async execute(name, email, passwd, phone, date) {
        const repository = new AuthRepository()
        const state = await repository.registerUser(name, email, passwd, phone, date)
        return state
    }
}