import { UserRepository } from "../repositories/user.repository";

export class PutUserUseCase{
    async execute(id,name,email,phone,date,token){
        const repository = new UserRepository()
        const state = await repository.putUser(id,name,email,phone,date,token)
        return state
    }
}