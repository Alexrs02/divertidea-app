import { BookingRepository } from "../repositories/booking.repository";


export class GetAllBookingUseCase{
    async execute(token){
        const repository = new BookingRepository()
        const state = await repository.getAllBookings(token)
        return state
    }
}