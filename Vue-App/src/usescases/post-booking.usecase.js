import { BookingRepository } from "../repositories/booking.repository";


export class PostBookingUseCase {
    async execute(booking, token){
        const repository = new BookingRepository()
        const state = await repository.postBooking(booking, token)
        return state
    }
}