import { BookingRepository } from "../repositories/booking.repository";


export class BookingsUseCase {
    async execute(userid, token){
        const repository = new BookingRepository()
        const state = await repository.getBookings(userid, token)
        return state
    }
}