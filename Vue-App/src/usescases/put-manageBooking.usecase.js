import { BookingRepository } from "../repositories/booking.repository";


export class PutBookingUseCase{
    async execute(isAccepted, id, token){
        const repository = new BookingRepository()
        const state = await repository.putManageBooking(isAccepted,id,token)
        return state
    }
}