import { createStore } from 'vuex';


export default createStore({
    state() {
        return {
            isLoggedIn: localStorage.getItem('isLoggedIn') === 'true',
            user: JSON.parse(localStorage.getItem('user')) || null,
            bookings: JSON.parse(localStorage.getItem('bookings')) || null
        };
    },
    mutations: {
        setLoggedIn(state, value) {
            state.isLoggedIn = value;
            localStorage.setItem('isLoggedIn', value.toString());
        },
        setUser(state, user) {
            state.user = user;
            localStorage.setItem('user', JSON.stringify(user));
        },
        setBookings(state, bookings) {
            state.bookings = bookings
            localStorage.setItem('bookings', JSON.stringify(bookings));
        }
    },
    actions: {
        login({ commit }, { user }) {
            // Lógica para iniciar sesión
            commit('setLoggedIn', true);
            commit('setUser', user);
        },
        showBookings({ commit }, { bookings }) {
            commit('setBookings', bookings)
        },
        logout({ commit }) {
            // Lógica para cerrar sesión
            localStorage.clear()
            commit('setUser', null);
            commit('setBookings', null)
            commit('setLoggedIn', false);
        }
    },
    getters: {
        isLoggedIn: state => state.isLoggedIn,
        user: state => state.user,
        bookings: state => state.bookings
    }
});