import { createRouter, createWebHistory } from 'vue-router'
import mainMiddleware from './middleware/main'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'info',
      component: () => import('../views/Info.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LogIn.vue')
    },
    {
      path: '/signin',
      name: 'signin',
      component: () => import('../views/SignIn.vue')
    },
    {
      path: '/rules',
      name: 'rules',
      component: () => import('../views/Rules.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/Profile.vue'),
    },
    {
      path: '/reserve',
      name: 'reserve',
      component: () => import('../views/Reserve.vue'),
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/manage-reserve',
      name: 'manage-reserve',
      component: () => import('../views/ManageReserve.vue'),
      meta: {
        requiresAuth: true,
        role: 'admin'
      }
    }
  ]
})
router.beforeEach(mainMiddleware);

export default router
