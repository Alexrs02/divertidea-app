// src/router/mainMiddleware.js
import store from '../../store';
const mainMiddleware = (to, from, next) => {
    const isProtected = to.matched.some(record => record.meta.requiresAuth);

    if (isProtected) {
        // check if the user is logged in
        if (!store.getters.user) {
            return next({ name: 'login' })
        }

        // check if the route requires a specific role
        if (to.meta.role) {
            const role = store.getters.user.role[0];
            if (role === to.meta.role) {
                return next()
            }else{
                return next({ name: 'info' })   
            }
        }
    }
    next()
}


export default mainMiddleware;